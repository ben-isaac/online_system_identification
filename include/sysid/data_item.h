/*
* Copyright (c) 2018 Carnegie Mellon University, Azarakhsh Keipour <akeipour@andrew.cmu.edu>
* This software was developed under a NASA contract NNX17CS56C-01.
*
*/

#ifndef __SYSID_DATA_ITEM_INCLUDED__
#define __SYSID_DATA_ITEM_INCLUDED__

#include <sysid/SignalData.h>
#include <sysid/FaultData.h>
#include <sysid/commons.h>

namespace sysid
{
    class DataItem
    {
    public:
        DataType Time;                  ///< The time when the data item (input/output pair) was recorded.
        DataType Desired;               ///< The desired (input) value of the system.
        DataType Measured;              ///< The measured (output) value of the system.
        DataType EstimatedNextStep;     ///< The estimated (predicted) value of the system for the given input.
        DataType EstimatedBackward;     ///< The estimated (predicted) value of the system for the given input.
        DataType EstimatedForward;     ///< The estimated (predicted) value of the system for the given input.
        
        ///
        /// Constructor for DataItem object.
        ///
        DataItem(DataType _time = 0, DataType _desired = 0, DataType _measured = 0, DataType _estimated_next_step = 0, DataType _estimated_backward = 0, DataType _estimated_forward = 0);

        ///
        /// Convert the data item to a message.
        ///
        SignalData ToROSMessage(double error_variance = -1.0F, std::string _frame_id = "", bool stabilized = true) const;

        enum ErrorType { NextStep = 0, BackwardPrediction = 1, ForwardPrediction = 2 };        
        ///
        /// Convert the data item to a ROS fault message.
        ///
        FaultData ToFaultROSMessage(DataType fault_threshold, DataType error_variance, ErrorType error_type_used, bool stabilized = true) const;
    };
} // end namespace sysid
# endif // __SYSID_DATA_ITEM_INCLUDED__
