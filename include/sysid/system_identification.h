/*
* Copyright (c) 2018 Carnegie Mellon University, Azarakhsh Keipour <akeipour@andrew.cmu.edu>
* This software was developed under a NASA contract NNX17CS56C-01.
*
*/

#ifndef __SYSID_SYSTEM_IDENTIFICATION_INCLUDED__
#define __SYSID_SYSTEM_IDENTIFICATION_INCLUDED__

#include <sysid/commons.h>
#include <sysid/matrix.h>
#include <sysid/transfer_function.h>
#include <list>

namespace sysid
{
    class SystemIdentification 
    {
    public:
        enum Methods {RLS, RIV};

        SystemIdentification();
        SystemIdentification(const SystemIdentification &sys);
        SystemIdentification(int input_order, int output_order, double forgetting_factor = 1, DataType initial_covariance = 1, int max_past_prediction = 25, Methods method = Methods::RLS);
        SystemIdentification& operator=(const SystemIdentification& sys);
        void Reset();
        int GetInputOrder();
        int GetOutputOrder();
        Matrix GetTheta();
        TransferFunction GetTransferFunction();
        DataType Step(DataType input_sample, DataType output_sample, bool update_model = true);
        DataType PredictFutureSteps(DataType inputSample, DataType lastPrediction, int pred_steps);
        DataType EstimateFromPastInputs(int pred_steps);
        bool IsCorrect();
        void SetMaxPredictionSize(int max_past_prediction);

    private:
        Matrix theta;
        int input_order, output_order;
        Methods method;
        double forgetting_factor;
        std::list<DataType> input_samples, output_samples;
        Matrix cov_matrix;
        
        int max_past_prediction;
        std::list<Matrix> past_thetas;
        std::list<Matrix> past_cov_matrices;
        std::list<DataType> past_inputs;
        std::list<DataType> past_outputs;
        std::list<DataType> past_predictions;
        
        DataType initial_covariance;
        
        static DataType rls_step(int rls_input_order, 
                          int rls_output_order,
                          double forget_factor, 
                          const std::list<DataType> &insamples, 
                          const std::list<DataType> &outsamples, 
                          Matrix *cov_mat, 
                          Matrix *rls_theta,
                          bool update_model = true);
        
        static DataType riv_step(int riv_rank, 
                          int riv_output_order,
                          double forget_factor, 
                          const std::list<DataType> &insamples, 
                          const std::list<DataType> &outsamples, 
                          Matrix *cov_mat, 
                          Matrix *riv_theta,
                          bool update_model = true);
    };
}
#endif // __SYSID_SYSTEM_IDENTIFICATION_INCLUDED__

