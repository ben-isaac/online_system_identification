/*
* Copyright (c) 2018 Carnegie Mellon University, Azarakhsh Keipour <akeipour@andrew.cmu.edu>
* This software was developed under a NASA contract NNX17CS56C-01.
*
*/

#ifndef __SYSID_COMMONS_INCLUDED__
#define __SYSID_COMMONS_INCLUDED__ 

#include <ros/ros.h>

namespace sysid
{
    extern bool DEBUG;

    ///
    /// The default data type for input/output data
    ///
    typedef double DataType;
    
    extern double variance_fault_threshold;
    
    extern double reset_after_seconds;

    
} // end namespace sysid
# endif // __SYSID_COMMONS_INCLUDED__
