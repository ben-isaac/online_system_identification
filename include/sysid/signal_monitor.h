/*
* Copyright (c) 2018 Carnegie Mellon University, Azarakhsh Keipour <akeipour@andrew.cmu.edu>
* This software was developed under a NASA contract NNX17CS56C-01.
*
*/

#ifndef __SYSID_SYSTEM_MONITOR_INCLUDED__
#define __SYSID_SYSTEM_MONITOR_INCLUDED__

#include <sysid/commons.h>
#include <sysid/transfer_function.h>
#include <sysid/system_identification.h>
#include <sysid/data_item.h>
#include <queue>
#include <cmath>
#include <string>
#include <ros/ros.h>
#include <mavros_msgs/NavDataPair.h>

//# define USE_BACKPRED_FOR_FAULT

namespace sysid
{
    class SignalMonitor
    {
    public:
        typedef SystemIdentification::Methods Methods;

        struct ResultType 
        {
            DataType NextStep;
            DataType BackwardPrediction;
            DataType ForwardPrediction;
            DataType NextStepError;
            DataType BackwardPredictionError;
            DataType ForwardPredictionError;
        };
        
    public:
        SystemIdentification SysID;
        std::string SignalName;
        ros::Time LastDataReceiveTime;

    public:
        SignalMonitor();
        SignalMonitor(int input_order, int output_order, double forgetting_factor = 1, DataType initial_covariance = 1, Methods method = Methods::RLS);        
        SignalMonitor(const SignalMonitor &sm);
        SignalMonitor& operator=(const SignalMonitor& sm);

        void Reset();
        void ActivateForwardPrediction(int steps);
        void ActivateBackwardPrediction(int steps);
        void DeactivateForwardPrediction();
        void DeactivateBackwardPrediction();
        int GetForwardPredictionSteps();
        int GetBackwardPredictionSteps();
        ResultType Step(DataType input_sample, DataType output_sample, bool update_model = true);
        void SetRosTopics(ros::NodeHandle &nh, std::string input_topic, std::string output_topic, std::string fault_topic, std::string signal_name);
        void DataReceivedCallback(const mavros_msgs::NavDataPairConstPtr& msg);
        void PublishData(const DataItem &data_item);
        void PublishFault(const DataItem &data_item);
        bool FaultDetected(DataType pred_error);
        bool IsStabilized();

    private:
        int forward_prediction_steps;
        int backward_prediction_steps;
        std::queue<DataType> pred_queue;
        ros::Subscriber subscriber;
        ros::Publisher data_publisher;
        ros::Publisher fault_publisher;

        DataType pred_err_var;
        DataType pred_err_mean;
        DataType pred_err_m2;
        bool is_theta_stable;
        bool has_measured_reached_commanded;
        int number_of_stable_samples;
        bool is_already_reset;

        #define stability_threshold 10e-2F
        #define min_stable_samples 100
        #define max_stability_error 0.1F
        #define min_input_value 2

    private:
        void update_error_variance(DataType prediction_error, bool update = true);
       
    };
}
#endif // __SYSID_SYSTEM_MONITOR_INCLUDED__


