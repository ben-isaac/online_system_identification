rosrun topic_tools relay_field /gt_temp /gt "std_msgs/Float64" "data: '4.5'" &

rosrun topic_tools relay_field /mavros/nav_info/roll /mavros/nav_info/roll_error "mavros_msgs/NavDataPair" "
commanded: m.commanded
measured: (m.measured - m.commanded)" &

rosrun topic_tools relay_field /mavros/nav_info/pitch /mavros/nav_info/pitch_error "mavros_msgs/NavDataPair" "
commanded: m.commanded
measured: (m.measured - m.commanded)" &

rosrun topic_tools relay_field /mavros/nav_info/airspeed /mavros/nav_info/airspeed_error "mavros_msgs/NavDataPair" "
commanded: m.commanded
measured: (m.measured - m.commanded)" &

rosrun topic_tools relay_field /mavros/nav_info/velocity /mavros/nav_info/velocity_x "mavros_msgs/NavDataPair" "
commanded: m.des_x
measured: m.meas_x" &

rosrun topic_tools relay_field /mavros/nav_info/velocity /mavros/nav_info/velocity_y "mavros_msgs/NavDataPair" "
commanded: m.des_y
measured: m.meas_y" &

rosrun topic_tools relay_field /mavros/nav_info/velocity /mavros/nav_info/velocity_x_error "mavros_msgs/NavDataPair" "
commanded: m.des_x
measured: (m.meas_x - m.des_x)" &

rosrun topic_tools relay_field /mavros/nav_info/velocity /mavros/nav_info/velocity_y_error "mavros_msgs/NavDataPair" "
commanded: m.des_y
measured: (m.meas_y - m.des_y)"
