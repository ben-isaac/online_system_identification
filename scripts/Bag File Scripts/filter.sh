rosbag filter $1 filt-$1 "topic != '/sysid/airspeed/data' and topic != '/sysid/airspeed/fault' and topic != '/sysid/roll/data' and topic != '/sysid/roll/fault' and topic != '/sysid/pitch/data' and topic != '/sysid/pitch/fault' and (topic != '/failure_status/engines' or (topic == '/failure_status/engines' and m.data == True))"
rm $1
mv filt-$1 $1

