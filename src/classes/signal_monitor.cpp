#include <sysid/signal_monitor.h>
#include <sysid/SignalData.h>
#include <sysid/FaultData.h>

namespace sysid
{
    SignalMonitor::SignalMonitor() :
        LastDataReceiveTime(ros::Time::now()),
        forward_prediction_steps(-1),
        backward_prediction_steps(-1),
        pred_err_var(0),
        pred_err_mean(0),
        pred_err_m2(0),
        is_theta_stable(false),
        has_measured_reached_commanded(false),
        number_of_stable_samples(0),
        is_already_reset(true)
    { }
    
    SignalMonitor::SignalMonitor (int input_order, int output_order, double forgetting_factor, DataType initial_covariance, Methods method) :
        SysID(input_order, output_order, forgetting_factor, initial_covariance, 0, method),
        LastDataReceiveTime(ros::Time::now()),
        forward_prediction_steps(-1),
        backward_prediction_steps(-1),
        pred_err_var(0),
        pred_err_mean(0),
        pred_err_m2(0),
        is_theta_stable(false),
        has_measured_reached_commanded(false),
        number_of_stable_samples(0),
        is_already_reset(true)
    { }
    
    SignalMonitor::SignalMonitor (const SignalMonitor &sm) :
        SysID(sm.SysID),
        LastDataReceiveTime(sm.LastDataReceiveTime),
        forward_prediction_steps(sm.forward_prediction_steps),
        backward_prediction_steps(sm.backward_prediction_steps),
        pred_err_var(sm.pred_err_var),
        pred_err_mean(sm.pred_err_mean),
        pred_err_m2(sm.pred_err_m2),
        is_theta_stable(sm.is_theta_stable),
        has_measured_reached_commanded(sm.has_measured_reached_commanded),
        number_of_stable_samples(sm.number_of_stable_samples),
        is_already_reset(sm.is_already_reset)
    { }
    
    SignalMonitor& SignalMonitor::operator=(const SignalMonitor& sm)
    {
        SysID = sm.SysID;
        LastDataReceiveTime = sm.LastDataReceiveTime;
        forward_prediction_steps = sm.forward_prediction_steps;
        backward_prediction_steps = sm.backward_prediction_steps;
        pred_err_var = sm.pred_err_var;
        pred_err_mean = sm.pred_err_mean;
        pred_err_m2 = sm.pred_err_m2;
        is_theta_stable = sm.is_theta_stable;
        has_measured_reached_commanded = sm.has_measured_reached_commanded;
        number_of_stable_samples = sm.number_of_stable_samples;
        is_already_reset = sm.is_already_reset;
        return *this;
    }
    
    void SignalMonitor::Reset()
    {
        if (is_already_reset == true) return;
        
        //if (DEBUG)
            ROS_WARN("%s is reset.", SignalName.c_str());

        SysID.Reset();
        pred_err_var = 0;
        pred_err_mean = 0;
        pred_err_m2 = 0;
        is_theta_stable = false;
        has_measured_reached_commanded = false;
        number_of_stable_samples = 0;
        std::queue<DataType>().swap(pred_queue); // clear the queue
        is_already_reset = true;
    }

    void SignalMonitor::ActivateForwardPrediction(int steps)
    {
        if (steps == forward_prediction_steps) return;
        std::queue<DataType>().swap(pred_queue); // clear the queue
        forward_prediction_steps = steps;
    }
    
    void SignalMonitor::ActivateBackwardPrediction(int steps)
    {
        if (steps == backward_prediction_steps) return;
        backward_prediction_steps = steps;
        SysID.SetMaxPredictionSize(steps);
    }
    
    void SignalMonitor::DeactivateForwardPrediction()
    {
        std::queue<DataType>().swap(pred_queue); // clear the queue
        forward_prediction_steps = -1;
    }
    
    void SignalMonitor::DeactivateBackwardPrediction()
    {
        backward_prediction_steps = -1;
    }

    int SignalMonitor::GetForwardPredictionSteps()
    {
        return forward_prediction_steps;
    }

    int SignalMonitor::GetBackwardPredictionSteps()
    {
        return backward_prediction_steps;
    }
    
    SignalMonitor::ResultType SignalMonitor::Step(DataType input_sample, DataType output_sample, bool update_model)
    {
        ResultType result;
        result.ForwardPrediction = 0;
        result.ForwardPredictionError = 0;
        result.NextStep = 0;
        result.NextStepError = std::abs(output_sample);

        if (has_measured_reached_commanded == false && std::abs(output_sample) > min_input_value)
            has_measured_reached_commanded = (std::abs(output_sample / input_sample - 1.0F) < max_stability_error);

        if (has_measured_reached_commanded == false)
            return result;

        Matrix prev_theta = SysID.GetTheta();
        
        // if (IsStabilized()) prev_theta.Trans().Print();
        
        result.NextStep = SysID.Step(input_sample, output_sample, update_model);
        result.NextStepError = std::abs(output_sample - result.NextStep);
        
        //if (IsStabilized())
        //{
        //    SysID.GetTheta().Trans().Print();
        //    int a = 2;
        //}
        
        if (forward_prediction_steps > 0)
        {
            pred_queue.push(SysID.PredictFutureSteps(input_sample, result.NextStep, forward_prediction_steps));
            if (pred_queue.size() == forward_prediction_steps)
            {
                result.ForwardPrediction = pred_queue.front();
                pred_queue.pop();
                result.ForwardPredictionError = std::abs(output_sample - result.ForwardPrediction);
            }
        }
        
        result.BackwardPrediction = 0;
        result.BackwardPredictionError = 0;
        if (backward_prediction_steps > 0)
        {
            result.BackwardPrediction = SysID.EstimateFromPastInputs(backward_prediction_steps);
            result.BackwardPredictionError = std::abs(output_sample - result.BackwardPrediction);
        }
        
        is_theta_stable = !update_model;
        if (update_model == true && has_measured_reached_commanded == true)
        {
            is_theta_stable = true;
            bool is_theta_zero = true;
            Matrix theta = SysID.GetTheta();
            for (int i = 1; (is_theta_stable == true || is_theta_zero == true) && i < theta.GetRows(); ++i)
            {
                if (prev_theta[i][0] - theta[i][0] > stability_threshold)
                    is_theta_stable = false;
                if (theta[i][0] != 0.0F)
                    is_theta_zero = false;
            }
            is_theta_stable &= !is_theta_zero;
        }

        update_error_variance(output_sample - result.NextStep, update_model);

        // if (DEBUG)
            if (IsStabilized() == false)
                ROS_INFO("%s\t%s %d", SignalName.c_str(), "Not Stabilized", number_of_stable_samples);

        if (DEBUG)
            ROS_INFO("%s\tPE Var: %0.3f\tNextErr: %0.3f\tBWPErr: %0.3f\tFWPErr: %0.3f", 
                        SignalName.c_str(), pred_err_var, output_sample - result.NextStep, 
                        output_sample - result.BackwardPrediction, output_sample - result.ForwardPrediction);

        return result;
    }
    
    void SignalMonitor::update_error_variance(DataType prediction_error, bool update)
    {
        if (update == false || has_measured_reached_commanded == false) return;
        
        if (is_theta_stable == false)
        {
            number_of_stable_samples = 0;
            pred_err_mean = 0;
            pred_err_var = 0;
            pred_err_m2 = 0;
            return;
        }

        // Use Welford's algorithm to update the prediction error variance
        // TODO: Change for weighted variance (for forget_factor < 1)
        number_of_stable_samples++;
        DataType delta = prediction_error - pred_err_mean;
        pred_err_mean = pred_err_mean + delta / number_of_stable_samples;
        DataType delta2 = prediction_error - pred_err_mean;
        pred_err_m2 += delta * delta2;
        pred_err_var = pred_err_m2 / number_of_stable_samples;
    }

    bool SignalMonitor::IsStabilized()
    {
        if (DEBUG)
            ROS_INFO("Num: %d", number_of_stable_samples);
        
        return ((has_measured_reached_commanded == true) && (number_of_stable_samples >= min_stable_samples));
    }

    void SignalMonitor::SetRosTopics(ros::NodeHandle &nh, std::string input_topic, std::string output_topic, std::string fault_topic, std::string signal_name)
    {
        // ROS_INFO("Publishing results for %s on topic \"%s\"", signal_name.c_str(), output_topic.c_str());
        SignalName = signal_name;
        data_publisher = nh.advertise<SignalData>(output_topic, 10);
        fault_publisher = nh.advertise<FaultData>(fault_topic, 10);
        subscriber = nh.subscribe(input_topic, 10, &SignalMonitor::DataReceivedCallback, this);
    }
    
    void SignalMonitor::DataReceivedCallback(const mavros_msgs::NavDataPairConstPtr& msg)
    {
        double time = msg->header.stamp.sec + double(msg->header.stamp.nsec) * 1e-9;
        LastDataReceiveTime = ros::Time::now();
        is_already_reset = false;
        DataItem data_item = DataItem(time, msg->commanded, msg->measured);
        ResultType estimation_result = Step(data_item.Desired, data_item.Measured);
        data_item.EstimatedNextStep = estimation_result.NextStep;
        data_item.EstimatedBackward = estimation_result.BackwardPrediction;
        data_item.EstimatedForward = estimation_result.ForwardPrediction;
        PublishData(data_item);
        
        #ifdef USE_BACKPRED_FOR_FAULT
            if (IsStabilized() && FaultDetected(data_item.Measured - estimation_result.BackwardPrediction)) 
                PublishFault(data_item);
        #else
            if (IsStabilized() && FaultDetected(data_item.Measured - data_item.EstimatedNextStep)) 
                PublishFault(data_item);
        #endif
    }
    
    void SignalMonitor::PublishData(const DataItem &data_item)
    {
        // Publish the results to the topic
        // TODO: Write results to a file
        data_publisher.publish(data_item.ToROSMessage(pred_err_var, SignalName, IsStabilized()));
    }
    
    void SignalMonitor::PublishFault(const DataItem &data_item)
    {
        #ifdef USE_BACKPRED_FOR_FAULT
            fault_publisher.publish(data_item.ToFaultROSMessage(stability_threshold, pred_err_var, DataItem::ErrorType::BackwardPrediction, IsStabilized()));
        #else
            fault_publisher.publish(data_item.ToFaultROSMessage(stability_threshold, pred_err_var, DataItem::ErrorType::NextStep, IsStabilized()));
        #endif
    }

    bool SignalMonitor::FaultDetected(DataType pred_error)
    {
        DataType variance = pred_err_var;
        if (std::abs(variance) < 1e-6F) return false;
        DataType std_dev = std::sqrt(variance);
        if (std::abs(pred_error) > variance_fault_threshold * std_dev)
            return true;
        return false;
    }

} // end namespace
