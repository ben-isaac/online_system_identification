#include <sysid/system_identification.h>
#include <iostream>
#include <ros/ros.h>

namespace sysid
{
    SystemIdentification::SystemIdentification() :
            SystemIdentification(2, 2)
    {   }
    
    SystemIdentification::SystemIdentification(const SystemIdentification &sys) :
            theta(sys.theta),
            input_order(sys.input_order),
            output_order(sys.output_order),
            method(sys.method),
            forgetting_factor(sys.forgetting_factor),
            input_samples(sys.input_samples),
            output_samples(sys.output_samples),
            cov_matrix(sys.cov_matrix),
            max_past_prediction(sys.max_past_prediction),
            initial_covariance(sys.initial_covariance)
    {   }

    SystemIdentification::SystemIdentification(int input_order, 
                                               int output_order, 
                                               double forgetting_factor, 
                                               DataType initial_covariance, 
                                               int max_past_prediction, 
                                               SystemIdentification::Methods method) :
            theta(input_order + output_order, 1),
            input_order(input_order), 
            output_order(output_order), 
            method(method),
            forgetting_factor(forgetting_factor),
            output_samples(output_order + 1, 0), 
            cov_matrix(input_order + output_order, input_order + output_order),
            max_past_prediction(max_past_prediction),
            initial_covariance(initial_covariance)
    {
        if (method == Methods::RLS)
            input_samples = std::list<DataType>(input_order + 1, 0);
        else
            input_samples = std::list<DataType>(2 * input_order + 1, 0);
        
        std::size_t min_rc = std::min(cov_matrix.GetRows(), cov_matrix.GetCols());
        for (int i = 0; i < min_rc; i++)
            cov_matrix[i][i] = initial_covariance;
    }

    SystemIdentification& SystemIdentification::operator=(const SystemIdentification& sys)
    {
        theta = sys.theta;
        input_order = sys.input_order;
        output_order = sys.output_order;
        method = sys.method;
        forgetting_factor = sys.forgetting_factor;
        input_samples = sys.input_samples;
        output_samples = sys.output_samples;
        cov_matrix = sys.cov_matrix;
        max_past_prediction = sys.max_past_prediction;
        initial_covariance = sys.initial_covariance;
        return *this;
    }
    
    void SystemIdentification::Reset()
    {
        theta.Reset();
        std::fill(input_samples.begin(), input_samples.end(), 0);
        std::fill(output_samples.begin(), output_samples.end(), 0);
        cov_matrix.Reset();

        std::size_t min_rc = std::min(cov_matrix.GetRows(), cov_matrix.GetCols());
        for (int i = 0; i < min_rc; i++)
            cov_matrix[i][i] = initial_covariance;
    }
    
    Matrix SystemIdentification::GetTheta()
    {
        return theta;
    }

    DataType SystemIdentification::Step(DataType input_sample, DataType output_sample, bool update_model) 
    {
        input_samples.push_front(input_sample);
        input_samples.pop_back();
        output_samples.push_front(output_sample);
        output_samples.pop_back();
        
        past_inputs.push_back(input_sample);
        if (past_inputs.size() > max_past_prediction + input_samples.size() - 1) 
            past_inputs.pop_front();
        past_outputs.push_back(output_sample);
        if (past_outputs.size() > max_past_prediction + output_samples.size() - 1) 
            past_outputs.pop_front();

        if (past_inputs.size() >= input_samples.size()) past_thetas.push_back(theta);
        if (past_thetas.size() > max_past_prediction) past_thetas.pop_front();
        
        if (past_inputs.size() >= input_samples.size()) past_cov_matrices.push_back(cov_matrix);
        if (past_cov_matrices.size() > max_past_prediction) past_cov_matrices.pop_front();
        
        DataType prediction;
        if (method == Methods::RLS)
            prediction = rls_step(input_order, output_order, forgetting_factor, input_samples, output_samples, &cov_matrix, &theta, update_model);
        else
            prediction = riv_step(input_order, output_order, forgetting_factor, input_samples, output_samples, &cov_matrix, &theta, update_model);
        
        if (past_inputs.size() >= input_samples.size()) past_predictions.push_back(prediction);
        if (past_predictions.size() > max_past_prediction) past_predictions.pop_front();
        
        return prediction;
    }
    
    DataType SystemIdentification::PredictFutureSteps(DataType inputSample, DataType lastPrediction, int pred_steps)
    {
        std::list<DataType> in_samples(input_samples);
        std::list<DataType> out_samples(output_samples);
        
        Matrix pred_theta(theta);
        Matrix pred_cov(cov_matrix);
        
        for (int i = 0; i < pred_steps; ++i)
        {
            in_samples.push_front(inputSample);
            in_samples.pop_back();
        
            out_samples.push_front(lastPrediction);
            out_samples.pop_back();

            if (method == Methods::RLS)
                lastPrediction = rls_step(input_order, output_order, forgetting_factor, in_samples, out_samples, &pred_cov, &pred_theta);
            else
                lastPrediction = riv_step(input_order, output_order, forgetting_factor, in_samples, out_samples, &pred_cov, &pred_theta);
        }
        return lastPrediction;
    }
    
    DataType SystemIdentification::EstimateFromPastInputs(int pred_steps)
    {
        //ROS_INFO("pin=%zu  \t  ps=%d  \t  ins=%zu", past_inputs.size(), pred_steps, input_samples.size());
        if (pred_steps <= 0 || (past_inputs.size() < pred_steps + input_samples.size() - 1))
            return 0;
        
        std::list<DataType>::const_iterator inp_iter = past_inputs.begin();
        std::list<DataType>::const_iterator out_iter = past_outputs.begin();
        std::list<Matrix>::const_iterator theta_iter = past_thetas.begin();
        std::list<Matrix>::const_iterator cov_iter = past_cov_matrices.begin();
        
        int start_ind = past_thetas.size() - pred_steps;
        //ROS_INFO("inp=%zu  \t  th=%zu  \t  ind=%d", past_inputs.size(), past_thetas.size(), start_ind);

        for (int i = 0; i < start_ind; ++i)
        {
            ++inp_iter;
            ++out_iter;
            ++theta_iter;
            ++cov_iter;
        }
        
        std::list<DataType> in_samples;
        for (int i = 0; i < input_samples.size(); ++i)
        {
            in_samples.push_front(*inp_iter);
            ++inp_iter;
        }
        
        std::list<DataType> out_samples;
        for (int i = 0; i < output_samples.size(); ++i)
        {
            out_samples.push_front(*out_iter);
            ++out_iter;
        }

        Matrix pred_theta(*theta_iter);
        Matrix pred_cov(*cov_iter);
        DataType lastPrediction = 0;
        for (int i = 0; i < pred_steps; ++i)
        {
            if (i > 0)
            {
                in_samples.push_front(*inp_iter);
                in_samples.pop_back();
                ++inp_iter;
                out_samples.push_front(lastPrediction);
                out_samples.pop_back();
            }
            
            if (DEBUG)
            {
                printf("\n--------- %d ------------\n", i);
                
                printf("Pred_cov: \n");
                pred_cov.Print();
                printf("Curr_cov:\n");
                cov_matrix.Print();

                printf("\nPred_theta: ");
                pred_theta.Trans().Print();
                printf("Curr_theta: ");
                theta.Trans().Print();

                printf("\nall_input: ");
                for (auto v : past_inputs) printf("%0.3f ", v);
                printf("\npred_input: ");
                for (auto v : in_samples) printf("%0.3f  ", v);
                printf("\ncurr_input: ");
                for (auto v : input_samples) printf("%0.3f  ", v);

                printf("\n\nall_output: ");
                for (auto v : past_outputs) printf("%0.3f ", v);
                printf("\npred_output: ");
                for (auto v : out_samples) printf("%0.3f  ", v);
                printf("\ncurr_output: ");
                for (auto v : output_samples) printf("%0.3f  ", v);

                printf("\n\npast_predictions: ");
                for (auto v : past_predictions) printf("%0.3f  ", v);
                printf("\n");
            }
            
            if (method == Methods::RLS)
                lastPrediction = rls_step(input_order, output_order, forgetting_factor, in_samples, out_samples, &pred_cov, &pred_theta, false);
            else
                lastPrediction = riv_step(input_order, output_order, forgetting_factor, in_samples, out_samples, &pred_cov, &pred_theta, false);

            if (DEBUG)
                printf("Pred: %0.3f\n", lastPrediction);
        }
        return lastPrediction;
    }

    TransferFunction SystemIdentification::GetTransferFunction()
    {
        TransferFunction tf(TransferFunction::Types::Discrete, input_order, output_order);

        for (int i = 0; i < output_order; ++i)
            tf.OutputCoeffs[i] = theta[i][0];

        for (int i = 0; i < input_order; ++i)
            tf.InputCoeffs[i] = theta[output_order + i][0];

//         for (int i = 0; i < cov_matrix.GetRows(); ++i)
//             for (int j = 0; j < cov_matrix.GetCols(); ++j)
//                 tf.CovarianceMatrix[i][j] = cov_matrix[i][j];

        return tf;
    }
    
    int SystemIdentification::GetInputOrder()
    {
        return input_order;
    }

    int SystemIdentification::GetOutputOrder()
    {
        return output_order;
    }

    bool SystemIdentification::IsCorrect()
    {
        return (cov_matrix.IsCorrect() && theta.IsCorrect());
    }

    DataType SystemIdentification::rls_step(int rls_input_order, 
                                            int rls_output_order, 
                                            double forget_factor, 
                                            const std::list<DataType> &insamples, 
                                            const std::list<DataType> &outsamples, 
                                            Matrix *cov_mat, 
                                            Matrix *rls_theta,
                                            bool update_model)
    {
        Matrix phi(rls_input_order + rls_output_order, 1);

        int i = 0;
        for (std::list<DataType>::const_iterator it = (++outsamples.begin()); it != outsamples.end(); ++it, ++i)
            phi[i][0] = -1 * (*it);
        for (std::list<DataType>::const_iterator it = (++insamples.begin()); it != insamples.end(); ++it, ++i)
            phi[i][0] = (*it);

        Matrix phi_T = phi.Trans();

        // Predicted output (y_hat)
        DataType predicted_output = (phi_T * (*rls_theta)).ToScalar();
        
        if (update_model == false)
            return predicted_output;
        
        //Error
        DataType error = outsamples.front() - predicted_output;

        //Dominator
        DataType dominator = forget_factor + (phi_T * (*cov_mat) * phi).ToScalar();

        if (dominator == 0) dominator = 0.0001;

        //New system parameters
        (*rls_theta) = (*rls_theta) + (*cov_mat) * phi * error / dominator;

        //New covariance matrix
        *cov_mat = (*cov_mat) - (*cov_mat) * phi * phi_T * (*cov_mat) / dominator;
            
        return predicted_output;
    }

    DataType sysid::SystemIdentification::riv_step (int riv_rank, 
                                                    int riv_output_order, 
                                                    double forget_factor, 
                                                    const std::list<sysid::DataType> &insamples, 
                                                    const std::list<sysid::DataType> &outsamples, 
                                                    Matrix* cov_mat, 
                                                    Matrix* riv_theta, 
                                                    bool update_model)
    {
        Matrix phi( riv_rank + riv_output_order, 1);

        int i = 0;
        for (std::list<DataType>::const_iterator it = (++outsamples.begin()); it != outsamples.end(); ++it, ++i)
            phi[i][0] = -1 * (*it);
        for (std::list<DataType>::const_iterator it = (++insamples.begin()); i < 2 * riv_rank && it != insamples.end(); ++it, ++i)
            phi[i][0] = (*it);

        Matrix z(2 * riv_rank, 1);
        i = 0;
        for (std::list<DataType>::const_iterator it = (++insamples.begin()); it != insamples.end(); ++it, ++i)
            z[i][0] = (*it);

        Matrix phi_T = phi.Trans();

        // Predicted output (y_hat)
        DataType predicted_output = (phi_T * (*riv_theta)).ToScalar();

        if (update_model == false)
            return predicted_output;

        //Error
        DataType error = outsamples.front() - predicted_output;

        //Dominator
        DataType dominator = forget_factor + (phi_T * (*cov_mat) * z).ToScalar();

        if (dominator == 0) dominator = 0.0001;

        //New system parameters
        (*riv_theta) = (*riv_theta) + (*cov_mat) * z * error / dominator;

        //New covariance matrix
        *cov_mat = (*cov_mat) - (*cov_mat) * z * phi_T * (*cov_mat) / dominator;
                        
        return predicted_output;
    }
    
    void SystemIdentification::SetMaxPredictionSize(int max_past_prediction)
    {
        this->max_past_prediction = max_past_prediction;

        while (past_inputs.size() > max_past_prediction + input_samples.size() - 1) past_inputs.pop_front();
        while (past_outputs.size() > max_past_prediction + output_samples.size() - 1) past_outputs.pop_front();
        while (past_thetas.size() > max_past_prediction) past_thetas.pop_front();
        while (past_cov_matrices.size() > max_past_prediction) past_cov_matrices.pop_front();
        while (past_predictions.size() > max_past_prediction) past_predictions.pop_front();
    }
}
