#include <ros/ros.h>
#include <sysid/data.h>

namespace sysid
{
    // Insert a new data item and return the index
    size_t Data::Insert(DataItem _item)
    {
        Items.push_back(_item);
        return Items.size();
    }
    
    // Overload index operator for data
    DataItem &Data::operator[](size_t _index)
    {
        if (_index >= this->Count()) 
            throw std::out_of_range("Error: Given index is greater than the number of data.");

        return Items[_index];
    }

    // Return the number of items
    size_t Data::Count()
    {
        return Items.size();
    }
    
    // Load data from a file and return the count
    size_t Data::LoadFromFile(std::string _filename)
    {
        this->Clear();
        
        std::ifstream file(_filename.c_str());
        try
        {
            if (!file.good())
            {
                ROS_ERROR("File \"%s\" does not exist!", _filename.c_str());
                return 0;
            }

            DataItem item;
            
            while (file >> item.Time >> item.Desired >> item.Measured) 
                this->Insert(item);

            file.close();
        }
        catch (int e)
        {
            ROS_ERROR("Exception #%i: Error loading data from file \"%s\"", e, _filename.c_str());
            file.close();
        }
        
        return this->Count();
    }
    
    // Save data to a file and return the count
    bool Data::SaveToFile(std::string _filename)
    {
        std::ofstream file(_filename.c_str());
        try
        {
            if (!file.good())
            {
                ROS_ERROR("Error opening file \"%s\"!", _filename.c_str());
                return false;
            }

            file << std::fixed;
            file.precision(3);
            for (int i = 0; i < this->Count(); ++i)
                file << Items[i].Time << '\t' << Items[i].Desired << '\t' << Items[i].Measured << '\t'
                    << Items[i].EstimatedNextStep << '\t' << Items[i].EstimatedBackward << '\t' << Items[i].EstimatedForward << std::endl;

            file.close();
        }
        catch (int e)
        {
            ROS_ERROR("Exception #%i: Error saving data to file \"%s\"", e, _filename.c_str());
            file.close();
        }
        return this->Count();
    }

    // Clear data
    void Data::Clear()
    {
        Items.clear();
    }
    
} // end namespace sysid
