#include <ros/ros.h>
#include <sysid/data.h>
#include <cmath>

namespace sysid
{
    // Constructor for the data item object
    DataItem::DataItem(DataType _time, DataType _desired, DataType _measured, DataType _estimated_next_step, DataType _estimated_backward, DataType _estimated_forward)
    {
        Time = _time;
        Desired = _desired;
        Measured = _measured;
        EstimatedNextStep = _estimated_next_step;
        EstimatedBackward = _estimated_backward;
        EstimatedForward = _estimated_forward;
    }
    
    // Convert the data item to a message
    SignalData DataItem::ToROSMessage(double error_variance, std::string _frame_id, bool stabilized) const
    {
        SignalData msg;
        msg.header.stamp = ros::Time::now();
        msg.header.frame_id = _frame_id;
        msg.time = this->Time;
        msg.desired = this->Desired;
        msg.measured = this->Measured;
        msg.estimated_step = this->EstimatedNextStep;
        msg.error_step = std::abs(this->EstimatedNextStep - this->Measured);
        msg.estimated_backward = this->EstimatedBackward;
        msg.error_backward = std::abs(this->EstimatedBackward - this->Measured);
        msg.estimated_forward = this->EstimatedForward;
        msg.error_forward = std::abs(this->EstimatedForward - this->Measured);
        msg.error_variance = error_variance;
        if (error_variance > 1e-6F && stabilized)
        {
            DataType std_dev = std::sqrt( error_variance );
            msg.var_error_step = (std::abs(msg.error_step) / std_dev);
            msg.var_error_forward = (std::abs(msg.error_forward) / std_dev);
            msg.var_error_backward = (std::abs(msg.error_backward) / std_dev);
        }
        else
        {
            msg.var_error_step = 0.0F;
            msg.var_error_forward = 0.0F;
            msg.var_error_backward = 0.0F;
        }
        msg.stabilized = (stabilized)? 1 : 0;
        return msg;
    }

    FaultData DataItem::ToFaultROSMessage(DataType fault_threshold, DataType error_variance, ErrorType error_type_used, bool stabilized) const
    {
        FaultData msg;
        msg.time = this->Time;
        msg.desired = this->Desired;
        msg.measured = this->Measured;
        msg.estimated_step = this->EstimatedNextStep;
        msg.error_step = this->Measured -this->EstimatedNextStep;
        msg.estimated_backward = this->EstimatedBackward;
        msg.error_backward = this->Measured - this->EstimatedBackward;
        msg.estimated_forward = this->EstimatedForward;
        msg.error_forward = this->Measured - this->EstimatedForward;
        msg.fault_threshold = fault_threshold;
        msg.error_variance = error_variance;
        msg.error_type_used = (int)(error_type_used);       // 0: NextStep   1: BackwardError    2: ForwardError
        msg.stabilized = (stabilized)? 1 : 0;
        return msg;
    }
} // end namespace sysid


