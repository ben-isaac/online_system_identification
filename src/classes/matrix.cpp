#include <sysid/matrix.h>
#include <iostream>

namespace sysid
{
    Matrix::Matrix() :
            rows(0), cols(0), data(NULL) 
            { }

    Matrix::Matrix(std::size_t nrows, std::size_t ncols) :
        rows(nrows), cols(ncols)
    {
        data = new DataType[rows * cols] {0};
    }

    Matrix::Matrix(DataType scalar) :
        rows(1), cols(1)
    {
        data = new DataType[1];
        data[0] = scalar;
    }

    Matrix::Matrix(const Matrix & mat) :
        rows(mat.rows), cols(mat.cols)
    {
        this->data = new DataType[this->cols * this->rows];
        for (int i = 0; i < this->rows; i++)
            for (int j = 0; j < this->cols; j++)
                (*this)[i][j] = mat[i][j];
    }

    Matrix::~Matrix() 
    {
        delete[] data;
    }

    void Matrix::Reset()
    {
        delete[] data;
        data = new DataType[rows * cols] {0};
    }
    
    DataType* sysid::Matrix::operator[](std::size_t row) const
    {
        return &(data[row * cols]);
    }

    Matrix Matrix::operator+(Matrix const& mat) const 
    {
        if (this->cols != mat.cols || this->rows != mat.rows)
            return mat;

        Matrix tempMat(this->rows, this->cols);
        for (int i = 0; i < this->rows; i++)
            for (int j = 0; j < this->cols; j++)
                tempMat[i][j] = (*this)[i][j] + mat[i][j];
        return tempMat;
    }

    Matrix Matrix::operator-(Matrix const& mat) const 
    {
        if (this->cols != mat.cols || this->rows != mat.rows)
            return mat;

        Matrix tempMat(this->rows, this->cols);
        for (int i = 0; i < this->rows; i++)
            for (int j = 0; j < this->cols; j++)
                tempMat[i][j] = (*this)[i][j] - mat[i][j];
        return tempMat;
    }

    Matrix Matrix::operator*(Matrix const& mat) const 
    {
        if (this->cols != mat.rows) {
            return mat;
        }
        Matrix tempMat(this->rows, mat.cols);

        for (int col = 0; col < mat.cols; col++)
            for (int row = 0; row < this->rows; row++) {
                DataType tempVal = 0;
                for (int iterator = 0; iterator < this->cols; iterator++)
                    tempVal += (*this)[row][iterator] * mat[iterator][col];
                tempMat[row][col] = tempVal;
            }

        return tempMat;
    }

    std::size_t Matrix::GetRows()
    {
        return rows;
    }

    std::size_t Matrix::GetCols()
    {
        return cols;
    }

    Matrix Matrix::operator*(DataType val) const 
    {
        Matrix tempMat(this->rows, this->cols);
        for (int i = 0; i < this->rows; i++)
            for (int j = 0; j < this->cols; j++)
                tempMat[i][j] = (*this)[i][j] * val;
        return tempMat;
    }

    Matrix& Matrix::operator=(const Matrix & mat)
    {
        if(this->data!=NULL) delete[] data;
        
        this->cols=mat.cols;
        this->rows=mat.rows;
        this->data=new DataType[this->cols*this->rows];
        for (int i = 0; i < this->rows; i++)
            for (int j = 0; j < this->cols; j++)
                (*this)[i][j] = mat[i][j];

        return *this;
    }

    Matrix operator*(DataType val, const Matrix& mat) 
    {
        return mat * val;
    }

    Matrix Matrix::Trans() const 
    {
        Matrix tempMat(this->cols, this->rows);
        for (int i = 0; i < this->rows; i++)
            for (int j = 0; j < this->cols; j++)
                tempMat[j][i] = (*this)[i][j];
        return tempMat;
    }

    DataType Matrix::ToScalar() 
    {
        return data[0];
    }

    Matrix Matrix::operator/(DataType val) const 
    {
        return (*this) * (1 / val);
    }

    void Matrix::Print() {
        for (int i = 0; i < rows; i++) 
        {
            for (int j = 0; j < cols; j++)
                printf("%4.7f\t", (*this)[i][j]);
            printf("\n");
        }
    }

    bool Matrix::IsCorrect()
    {
        for (int r = 0; r < rows; r++)
            for (int c = 0; c < cols; c++)
                if ((*this)[r][c] != (*this)[r][c])
                    return false;
        return true;
    }
}
