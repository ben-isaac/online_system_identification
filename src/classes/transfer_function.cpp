#include <sysid/transfer_function.h>

namespace sysid
{
    TransferFunction::TransferFunction() :
        Type(Types::Discrete), InputSize(0), OutputSize(0)
    { }
    
    TransferFunction::TransferFunction(Types type, std::size_t input_size, std::size_t output_size) :
        Type(type), InputSize(input_size), OutputSize(output_size), 
        InputCoeffs(input_size), OutputCoeffs(output_size)
        //CovarianceMatrix(input_size + output_size, std::vector<DataType>(input_size + output_size))
    { }

    void TransferFunction::Print()
    {
        if (Type == Types::Discrete)
            print_discrete();
        else
            print_continuous();
    }
        
    void TransferFunction::print_continuous()
    {
        // TODO: Complete the print_continuous function
        
        std::cout << "Printing continuous transfer functions is not supported yet!" << std::endl;
    }
    
    void TransferFunction::print_discrete()
    {
        std::string numerator;
        for (int i = 0; i < InputSize; ++i)
        {
            if (i > 0)
                numerator += (InputCoeffs[i] >= 0)? "  +  " : "  -  ";
            
            char str[100];
            sprintf(str, "%0.4f z^[-%d]", std::abs(InputCoeffs[i]), i + 1);
            numerator += str;
        }

        std::string denominator = "1";
        for (int i = 0; i < OutputSize; ++i)
        {
            denominator += (OutputCoeffs[i] >= 0)? "  +  " : "  -  ";
            char str[100];
            sprintf(str, "%0.4f z^[-%d]", std::abs(OutputCoeffs[i]), i + 1);
            denominator += str;
        }

        std::string frac_line(std::max(numerator.length(), denominator.length()), '-');
        
        for (int i = 0; i < (frac_line.length() - numerator.length()) / 2; ++i)
            numerator = " " + numerator;
        
        for (int i = 0; i < (frac_line.length() - denominator.length()) / 2; ++i)
            denominator = " " + denominator;
        
        if (OutputSize > 0)
        {
            std::cout << "    " << numerator << std::endl;
            std::cout << "Y = " << frac_line << " U" << std::endl;
            std::cout << "    " << denominator << std::endl;
        }
        else
            std::cout << "Y = (" << numerator << ") U" << std::endl;
        
        //std::cout << "Output Error Variance = " << PredictionErrorVariance << std::endl;
    }

} // end namespace sysid

