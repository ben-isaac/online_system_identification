#include <ros/ros.h>
#include <string>
#include <vector>
#include <sysid/data.h>
#include <unistd.h>
#include <errno.h>
#include <mavros_msgs/NavDataPair.h>
#include <sysid/SignalData.h>
#include <sysid/signal_monitor.h>
#include <mavros_msgs/ParamGet.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Bool.h>
#include <sysid/commons.h>

struct SignalTopic
{
    std::string Input;
    std::string Output;
    std::string Fault;
    std::string Name;
};

std::vector<SignalTopic> ReadSignalTopics()
{
    std::vector<SignalTopic> topics;
    for (int i = 0; true; ++i)
    {
        std::string str_topic_i = "/sysid/topic" + std::to_string(i + 1) + "/";
        
        SignalTopic topic;
        ros::param::param<std::string>(str_topic_i + "input", topic.Input, "");
        if (topic.Input == "") break;

        bool enable_topic = false;
        ros::param::param<bool>(str_topic_i + "enable", enable_topic, false);
        if (enable_topic == false) continue;

        ros::param::param<std::string>(str_topic_i + "output", topic.Output, "");
        ros::param::param<std::string>(str_topic_i + "name", topic.Name, "");
        ros::param::param<std::string>(str_topic_i + "fault", topic.Fault, "");
        topics.push_back(topic);
    }
    return topics;
}

ros::ServiceClient throttle_status_client;
ros::ServiceClient aileron_status_client;
ros::ServiceClient rudder_status_client;
ros::ServiceClient elevator_status_client;

ros::Publisher throttle_status_publisher;
ros::Publisher aileron_status_publisher;
ros::Publisher rudder_status_publisher;
ros::Publisher elevator_status_publisher;

void ThrottleTimerCallback(const ros::TimerEvent& timev)
{
    mavros_msgs::ParamGet srv;
    srv.request.param_id = "DISABLE_THROTTLE";
    std_msgs::Bool msg;

    if (throttle_status_client.call(srv) && srv.response.success == true)
        msg.data = srv.response.value.integer;
    
    throttle_status_publisher.publish(msg);
}

void AileronTimerCallback(const ros::TimerEvent& timev)
{
    mavros_msgs::ParamGet srv;
    srv.request.param_id = "DISABLE_AILERON";
    std_msgs::Int8 msg;

    if (aileron_status_client.call(srv) && srv.response.success == true)
        msg.data = srv.response.value.integer;

    aileron_status_publisher.publish(msg);
}

void RudderTimerCallback(const ros::TimerEvent& timev)
{
    mavros_msgs::ParamGet srv;
    srv.request.param_id = "DISABLE_RUDDER";
    std_msgs::Int8 msg;

    if (rudder_status_client.call(srv) && srv.response.success == true)
        msg.data = srv.response.value.integer;

    rudder_status_publisher.publish(msg);
}

void ElevatorTimerCallback(const ros::TimerEvent& timev)
{
    mavros_msgs::ParamGet srv;
    srv.request.param_id = "DISABLE_ELEVATOR";
    std_msgs::Int8 msg;

    if (elevator_status_client.call(srv) && srv.response.success == true)
        msg.data = srv.response.value.integer;

    elevator_status_publisher.publish(msg);
}

std::vector<sysid::SignalMonitor*> signal_monitors;

void ResetTimerCallback(const ros::TimerEvent& timev)
{
    for (int i = 0; i < signal_monitors.size(); ++i)
        if (ros::Time::now() > signal_monitors[i]->LastDataReceiveTime + ros::Duration(sysid::reset_after_seconds))
            signal_monitors[i]->Reset();
}

int main(int argc, char **argv)
{
    // Initialize the ROS node
    ros::init(argc, argv, "sysid");
    ros::NodeHandle nh;

    /*********************** Read ROS Parameters ********************************/

    // Read the input and output topics
    std::vector<SignalTopic> sysid_topics = ReadSignalTopics();
    
    // Read the sysid method parameters
    int sysid_input_order, sysid_output_order;
    ros::param::param<int>("/sysid/sysid_input_order", sysid_input_order, 3);
    ros::param::param<int>("/sysid/sysid_output_order", sysid_output_order, 3);
    std::string sysid_method;
    ros::param::param<std::string>("/sysid/sysid_method", sysid_method, "RLS");
    sysid::SystemIdentification::Methods sysid_method_type = sysid::SystemIdentification::Methods::RLS;
    if (sysid_method == "RIV")
        sysid_method_type = sysid::SystemIdentification::Methods::RIV;
    ros::param::param<double>("/sysid/variance_fault_threshold", sysid::variance_fault_threshold, 5.0F);
    int backward_prediction_samples;
    ros::param::param<int>("/sysid/backward_prediction_samples", backward_prediction_samples, 50);
    ros::param::param<double>("/sysid/reset_after_seconds", sysid::reset_after_seconds, 3.0F);

    /*********************** End of ROS Parameter Reading ************************/
    
    // Define a timer to publish failure status from autopilot
    // TODO: Definitely remove from here and add to MAVROS node
    ros::Timer timer_throttle_status = nh.createTimer(ros::Duration(0.5F), ThrottleTimerCallback);
    ros::Timer timer_aileron_status = nh.createTimer(ros::Duration(0.5F), AileronTimerCallback);
    ros::Timer timer_rudder_status = nh.createTimer(ros::Duration(0.5F), RudderTimerCallback);
    ros::Timer timer_elevator_status = nh.createTimer(ros::Duration(0.5F), ElevatorTimerCallback);

    // Define a timer to reset estimation after specified seconds
    ros::Timer timer_reset_estimation = nh.createTimer(ros::Duration(1.0F), ResetTimerCallback);

    // Define a service and publisher for failure status
    throttle_status_client = nh.serviceClient<mavros_msgs::ParamGet>("/mavros/param/get");
    aileron_status_client = nh.serviceClient<mavros_msgs::ParamGet>("/mavros/param/get");
    rudder_status_client = nh.serviceClient<mavros_msgs::ParamGet>("/mavros/param/get");
    elevator_status_client = nh.serviceClient<mavros_msgs::ParamGet>("/mavros/param/get");

    throttle_status_publisher = nh.advertise<std_msgs::Bool>("/failure_status/engines", 10);
    aileron_status_publisher = nh.advertise<std_msgs::Int8>("/failure_status/aileron", 10);
    rudder_status_publisher = nh.advertise<std_msgs::Int8>("/failure_status/rudder", 10);
    elevator_status_publisher = nh.advertise<std_msgs::Int8>("/failure_status/elevator", 10);

    // Define the system identification objects
    for (int i = 0; i < sysid_topics.size(); ++i)
    {
        signal_monitors.push_back(new sysid::SignalMonitor(sysid_input_order, sysid_output_order, 1, 10e6, sysid_method_type));
        signal_monitors[i]->SetRosTopics(nh, sysid_topics[i].Input, sysid_topics[i].Output, sysid_topics[i].Fault, sysid_topics[i].Name);
        signal_monitors[i]->ActivateBackwardPrediction(backward_prediction_samples);
    }
    
    ros::spin();

    return 0;
}
